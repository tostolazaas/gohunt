package main

import(
	"github.com/go-ping/ping"
	"fmt"
	"flag"
)

func main(){
	direccion := flag.String("ip", "", "IP of the victim")
	flag.Parse()
	_, err := ping.NewPinger(*direccion)
	if err != nil {
		fmt.Println("No connected")
	}else{
		fmt.Println("Connected")
	}

	
}